package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

var Version = "1.0.1"

var Green = "\033[32m"
var Bold = "\033[1m"
var Reset = "\033[0m"

func main() {
	fmt.Println(` _             _____
| |           |____ |
| |____      __   / /_   _
| '_ \ \ /\ / /   \ \ | | |
| |_) \ V  V /.___/ / |_| |
|_.__/ \_/\_/ \____/ \__,_|`)

	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
		return
	}
	fmt.Println("\nbw3u's VSCodium Backup Script")
	fmt.Printf("\n%s%s=>%s Version: %s", Green, Bold, Reset, Version)
	fmt.Printf("\n%s%s=>%s Home Directory: %s\n\n", Green, Bold, Reset, dirname)
	fmt.Println(`* c - create a backup for your settings.
* e - create extension list for later use.
* l - copy your settings from current backup.
* i - install your extensions from extension list.
* q - quit from script.`)

	fmt.Println("\n[your input]: ")

	reader := bufio.NewReader(os.Stdin)
	char, _, err := reader.ReadRune()

	if err != nil {
		fmt.Println(err)
	}

	switch char {
	case 'c':
		copyCodiumSettings(dirname + `/.config/VSCodium/User/settings.json`)
		fmt.Printf("%s%s=>%s Finished job.\n", Green, Bold, Reset)
		break
	case 'e':
		createExtensionList()
		fmt.Printf("%s%s=>%s Finished job.\n", Green, Bold, Reset)
		break
	case 'l':
		loadBackup()
		fmt.Printf("%s%s=>%s Finished job.\n", Green, Bold, Reset)
		break
	case 'i':
		installExtensions()
		fmt.Printf("%s%s=>%s Finished job.\n", Green, Bold, Reset)
		break
	case 'q':
		fmt.Println("\nExit.")
		break
	default:
		fmt.Println("\nWrong input, please run the script again.")
		return
	}

	return
}

func copyCodiumSettings(settingsPath string) (err error) {
	src, err := os.Stat(settingsPath)
	if err != nil {
		log.Fatal(err)
		return
	}

	if !src.Mode().IsRegular() {
		return fmt.Errorf("%s%s=>%s Non-regular source file %s (%q)", Green, Bold, Reset, src.Name(), src.Mode().String())
	}

	fmt.Printf("%s%s=>%s VSCodium settings found at: %s\n", Green, Bold, Reset, settingsPath)

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
		return
	}

	destDir := wd + `/.config/VSCodium/User`
	fmt.Printf("%s%s=>%s Backup destination directory: %s\n", Green, Bold, Reset, destDir)

	if _, err := os.Stat(destDir); os.IsNotExist(err) {
		fmt.Printf("%s%s=>%s Backup destination directory does not exist, creating it hold on.\n", Green, Bold, Reset)
		if err := os.MkdirAll(destDir, 0755); err != nil {
			log.Fatalln(err)
		}
	}

	input, err := ioutil.ReadFile(settingsPath)
	if err != nil {
		log.Fatal(err)
		return
	}

	err = ioutil.WriteFile(destDir+`/settings.json`, input, 0755)
	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Printf("%s%s=>%s Settings backup successfully created.\n", Green, Bold, Reset)

	return
}

func createExtensionList() {
	cmd := exec.Command("codium", "--list-extensions")

	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	extFile := "codiumExtensions.md"

	os.Remove(extFile)

	ext, err := os.Create(extFile)
	if err != nil {
		log.Fatal(err)
	}

	defer ext.Close()

	fmt.Fprintln(ext, string(stdout))

	fmt.Printf("%s%s=>%s Extensions that are currently installed is printed to %s.\n", Green, Bold, Reset, extFile)

	return
}

func loadBackup() {
	src, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
		return
	}

	input, err := ioutil.ReadFile(src + `/.config/VSCodium/User/settings.json`)
	if err != nil {
		log.Fatal(err)
		return
	}

	dest, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
		return
	}

	err = ioutil.WriteFile(dest+`/.config/VSCodium/User/settings.json`, input, 0755)
	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Printf("%s%s=>%s Settings backup loaded successfuly.\n", Green, Bold, Reset)
}

func installExtensions() {
	fmt.Printf("%s%s=>%s Starting installing extensions.\n", Green, Bold, Reset)

	cmd := exec.Command("bash", "-c", `while read line;do codium --install-extension "$line";done<codiumExtensions.md`)

	pipe, _ := cmd.StdoutPipe()
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
		return
	}
	reader := bufio.NewReader(pipe)
	line, err := reader.ReadString('\n')
	for err == nil {
		fmt.Println(line)
		line, err = reader.ReadString('\n')
	}
	cmd.Wait()

	fmt.Printf("%s%s=>%s Extensions are successfully installed.\n", Green, Bold, Reset)
	return
}
